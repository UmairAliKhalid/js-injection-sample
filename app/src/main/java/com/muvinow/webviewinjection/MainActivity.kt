package com.muvinow.webviewinjection

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.RelativeLayout
import android.webkit.WebViewClient
import java.io.BufferedReader
import android.view.Window
import android.view.WindowManager


/**
 * Created by Umair Khalid on 11/17/2018.
 */

class MainActivity : AppCompatActivity() {
    lateinit var mainView: RelativeLayout;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main)
        mainView = findViewById(R.id.parent_layout);

        val wv = ViewUtils.createWebView(this);
        val progress = ViewUtils.createProgressBar(this);
        progress?.centerProgress()

        if (wv != null && progress != null) {
            wv.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    injectJS(wv)
                    progress.visibility = View.GONE
                }
            }
            // at this point we will load
            wv.loadUrl("https://www.google.com")
            mainView.addView(wv)
            mainView.addView(progress)
        }
    }

    // This method will open js file from assets folder in application
    // and will inject that js file into web page currently visible to user
    private fun injectJS(webView: WebView) {
        try {
            val inputStream = assets.open("script.js")
            inputStream.bufferedReader().use(BufferedReader::readText)
        } catch (e: Exception) {
            null
        }?.let { webView.loadUrl("javascript:($it)()") }
    }
}
