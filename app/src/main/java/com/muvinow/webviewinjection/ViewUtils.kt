package com.muvinow.webviewinjection

import android.app.Activity
import android.content.Context
import android.support.annotation.Nullable
import android.webkit.WebView
import android.widget.ProgressBar



/**
 * Created by Umair Khalid on 11/17/2018.
 */

public class ViewUtils {

    companion object {
        //This method gonna create webview dynamically
        @Nullable
        public fun createWebView(context: Context?): WebView? {
            if (context == null)
                return null;

            val webView = WebView(context);
            webView.setFullScreenParams()
            webView.settings.javaScriptEnabled = true;
            return webView;
        }

        @Nullable
        public fun createProgressBar(context: Activity?): ProgressBar? {
            if (context == null)
                return null;
            return ProgressBar(context, null, android.R.attr.progressBarStyle)
        }
    }

}